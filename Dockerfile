FROM anapsix/alpine-java
COPY target/k8sdockertutorial-0.0.1-SNAPSHOT.jar /opt
ENV JAVA_OPTS=""
ENTRYPOINT ["sh","-c","java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /opt/k8sdockertutorial-0.0.1-SNAPSHOT.jar" ]
EXPOSE 8080