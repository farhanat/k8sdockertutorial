package com.tutorial.k8s.controller;

import com.tutorial.k8s.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author farhan
 */
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping(value = "employees")
    public ResponseEntity<?> getAllEmployee() {
        return ResponseEntity.ok(employeeRepository.findAll());
    }

}
