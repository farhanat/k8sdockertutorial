package com.tutorial.k8s.repository;

import com.tutorial.k8s.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author farhan
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
