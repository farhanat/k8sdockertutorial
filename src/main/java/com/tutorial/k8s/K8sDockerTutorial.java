package com.tutorial.k8s;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sDockerTutorial {

	public static void main(String[] args) {
		SpringApplication.run(K8sDockerTutorial.class, args);
	}

}
